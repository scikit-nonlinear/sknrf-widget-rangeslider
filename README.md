# sknrf-widget-qrangeslider

<img src="qtrangeslider.png"><center>Qt Range Slider</center>

Qt Progress Indicator.

This module is used to:

    - Render a Range Slider for a tunable value.

## Build-Flow

### Install Dependencies

See requirements.txt

### Release Build

```bash
rm -rf build ; mkdir -p build ; cd build
cmake ..
make
sudo make install
```

### Debug Build

```bash
rm -rf build ; mkdir -p build ; cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DSKNRF_BUILD_DOC=ON -DSKNRF_BUILD_EXTRA=ON -DSKNRF_BUILD_PLUGIN=ON -DSKNRF_BUILD_PY=ON -DSKNRF_BUILD_TEST=ON ..
make
sudo make install
```

### Example

```bash
python3 main.py
```

