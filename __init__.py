from pathlib import Path
import ctypes

lib_dir = Path(__file__).resolve().parent
lib = ctypes.CDLL(str(lib_dir / 'qrangeslider.so'))


from .qrangeslider import QRangeSlider
