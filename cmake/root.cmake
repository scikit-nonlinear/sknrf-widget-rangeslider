cmake_minimum_required(VERSION 3.26.0)
# Global Variables
## Version.
set(SKNRF_VERSION_MAJOR 1)
set(SKNRF_VERSION_MINOR 1)
set(SKNRF_VERSION_PATCH 1)
set(SKNRF_VERSION ${SKNRF_VERSION_MAJOR}.${SKNRF_VERSION_MINOR}.${SKNRF_VERSION_PATCH})
configure_file (
    "${CMAKE_CURRENT_LIST_DIR}/../sknrfconfig.h.in"
    "${CMAKE_CURRENT_BINARY_DIR}/sknrfconfig.h"
  )
include_directories("${CMAKE_BINARY_DIR}")
## Variables
if (CMAKE_SYSTEM_NAME MATCHES "Windows")
  set(SEP "\\")
  set(PATH_SEP "\\;")
else ()
  set(SEP "/")
  set(PATH_SEP ":")
endif()
set(CONDA_PREFIX $ENV{CONDA_PREFIX})
set(QTDIR $ENV{QTDIR})
set(CMAKE_PREFIX_PATH $ENV{CMAKE_PREFIX_PATH})

## CMake
if(WIN32)
    set(CMAKE_INSTALL_PREFIX ${CONDA_PREFIX}/Library)
else()   
    set(CMAKE_INSTALL_PREFIX ${CONDA_PREFIX})
endif()
set(CMAKE_SKIP_BUILD_RPATH FALSE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
set(CMAKE_INSTALL_RPATH ${CMAKE_INSTALL_PREFIX}/lib ${QTDIR}/lib)
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

## C++
set(CMAKE_CXX_STANDARD 17)

## Qt
find_package(Qt6 REQUIRED COMPONENTS Core Gui Widgets)
string(REPLACE "." ";" Qt_VERSION ${Qt6Widgets_VERSION})
list(GET Qt_VERSION 0 Qt_VERSION_MAJOR)
list(GET Qt_VERSION 1 Qt_VERSION_MINOR)
list(GET Qt_VERSION 2 Qt_VERSION_PATCH)

## Python (see CMake/Modules/FindPython/Support.cmake)
find_package (Python COMPONENTS Interpreter Development)
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 0 ${_PYTHON_PREFIX}_INTERPRETER_ID)
#
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 1 ${_PYTHON_PREFIX}_VERSION_MAJOR)
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 2 ${_PYTHON_PREFIX}_VERSION_MINOR)
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 3 ${_PYTHON_PREFIX}_VERSION_PATCH)
#
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 4 _${_PYTHON_PREFIX}_ARCH)
#
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 5 _${_PYTHON_PREFIX}_ABIFLAGS)
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 6 ${_PYTHON_PREFIX}_SOABI)
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 7 ${_PYTHON_PREFIX}_SOSABI)
#
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 8 ${_PYTHON_PREFIX}_STDLIB)
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 9 ${_PYTHON_PREFIX}_STDARCH)
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 10 ${_PYTHON_PREFIX}_SITELIB)
#list (GET _${_PYTHON_PREFIX}_INTERPRETER_PROPERTIES 11 ${_PYTHON_PREFIX}_SITEARCH)

# ================================== Shiboken detection ======================================
# Use provided python interpreter if given.
if(NOT python_interpreter)
    if(WIN32 AND "${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
        find_program(python_interpreter "python_d")
        if(NOT python_interpreter)
            message(FATAL_ERROR
                "A debug Python interpreter could not be found, which is a requirement when "
                "building this example in a debug configuration. Make sure python_d.exe is in "
                "PATH.")
        endif()
    else()
        find_program(python_interpreter "python")
        if(NOT python_interpreter)
            message(FATAL_ERROR
                "No Python interpreter could be found. Make sure python is in PATH.")
        endif()
    endif()
endif()
message(STATUS "Using python interpreter: ${python_interpreter}")
message(STATUS "Using Python_LIBRARIES: ${Python_LIBRARIES}")
message(STATUS "Using Python_INCLUDE_DIRS: ${Python_INCLUDE_DIRS}")
list (GET _Python_INTERPRETER_PROPERTIES 11 _Python_SITEARCH)
message(STATUS "Using _Python_SITEARCH: ${_Python_SITEARCH}")

# Macro to get various pyside / python include / link flags and paths.
# Uses the not entirely supported utils/pyside_config.py file.
macro(pyside_config option output_var)
    if(${ARGC} GREATER 2)
        set(is_list ${ARGV2})
    else()
        set(is_list "")
    endif()

    execute_process(
      COMMAND ${python_interpreter} "${_Python_SITEARCH}/PySide${Qt_VERSION_MAJOR}/examples/utils/pyside_config.py"
              ${option}
      OUTPUT_VARIABLE ${output_var}
      OUTPUT_STRIP_TRAILING_WHITESPACE)

    if ("${${output_var}}" STREQUAL "")
        message(FATAL_ERROR "Error: Calling pyside_config.py ${option} returned no output.")
    endif()
    if(is_list)
        string (REPLACE " " ";" ${output_var} "${${output_var}}")
    endif()
endmacro()

# Query for the shiboken generator path, Python path, include paths and linker flags.
# pyside_config(--shiboken-module-path shiboken_module_path)
set(shiboken_module_path ${_Python_SITEARCH}/shiboken${Qt_VERSION_MAJOR})
if(NOT EXISTS ${shiboken_module_path})
    message(FATAL_ERROR "shiboken_module_path not found")
endif()
message(STATUS "Found shiboken_module_path: ${shiboken_module_path}")

# pyside_config(--shiboken-generator-path shiboken_generator_path)
set(shiboken_generator_path ${_Python_SITEARCH}/shiboken${Qt_VERSION_MAJOR}_generator)
if(NOT EXISTS ${shiboken_generator_path})
    message(FATAL_ERROR "shiboken_generator_path not found")
endif()
message(STATUS "Found shiboken_generator_path: ${shiboken_generator_path}")

# pyside_config(--pyside-path pyside_path)
set(pyside_path ${CMAKE_SYSROOT}/usr/share/PySide${Qt_VERSION_MAJOR})
if(NOT EXISTS ${pyside_path})
    set(pyside_path ${_Python_SITEARCH}/PySide${Qt_VERSION_MAJOR})
endif()
if(NOT EXISTS ${pyside_path})
    message(FATAL_ERROR "pyside_path not found")
endif()
message(STATUS "Found pyside_path: ${pyside_path}")

# pyside_config(--pyside-typesystem-path pyside_typesystem_dir 1)
set(pyside_typesystem_dir ${CMAKE_SYSROOT}/usr/share/PySide${Qt_VERSION_MAJOR}/typesystems)
if(NOT EXISTS ${pyside_typesystem_dir})
    set(pyside_typesystem_dir ${_Python_SITEARCH}/PySide${Qt_VERSION_MAJOR}/typesystems)
endif()
if(NOT EXISTS ${pyside_typesystem_dir})
    message(FATAL_ERROR "pyside_typesystem_dir not found")
endif()
message(STATUS "Found pyside_typesystem_dir: ${pyside_typesystem_dir}")

# pyside_config(--pyside-include-path pyside_include_dir 1)
set(pyside_include_dir ${CMAKE_SYSROOT}/usr/include/PySide${Qt_VERSION_MAJOR})
if(NOT EXISTS ${pyside_include_dir})
    set(pyside_include_dir ${_Python_SITEARCH}/PySide${Qt_VERSION_MAJOR}/include)
endif()
if(NOT EXISTS ${pyside_include_dir})
    message(FATAL_ERROR "pyside_include_dir not found")
endif()
message(STATUS "Found pyside_include_dir: ${pyside_include_dir}")

# pyside_config(--shiboken-generator-include-path shiboken_include_dir 1)
set(shiboken_include_dir ${CMAKE_SYSROOT}/usr/include/shiboken${Qt_VERSION_MAJOR})
if(NOT EXISTS ${shiboken_include_dir})
set(shiboken_include_dir ${_Python_SITEARCH}/shiboken${Qt_VERSION_MAJOR}_generator/include)
endif()
if(NOT EXISTS ${shiboken_include_dir})
    message(FATAL_ERROR "shiboken_include_dir not found")
endif()
message(STATUS "Found shiboken_include_dir: ${shiboken_include_dir}")

# pyside_config(--shiboken-module-shared-libraries-cmake shiboken_shared_libraries 0)
set(shiboken_shared_libraries ${CMAKE_SYSROOT}/usr/lib/libshiboken${Qt_VERSION_MAJOR}.abi${Python_VERSION_MAJOR}.so.${Qt6Widgets_VERSION})
if(NOT EXISTS ${shiboken_shared_libraries})
set(shiboken_shared_libraries ${_Python_SITEARCH}/shiboken${Qt_VERSION_MAJOR}/libshiboken${Qt_VERSION_MAJOR}.abi${Python_VERSION_MAJOR}.so.${Qt_VERSION_MAJOR}.${Qt_VERSION_MINOR})
endif()
if(NOT EXISTS ${shiboken_shared_libraries})
    message(FATAL_ERROR "shiboken_shared_libraries not found")
endif()
message(STATUS "Found shiboken_shared_libraries: ${shiboken_shared_libraries}")

# pyside_config(--pyside-shared-libraries-cmake pyside_shared_libraries 0)
set(pyside_shared_libraries ${CMAKE_SYSROOT}/usr/lib/libpyside${Qt_VERSION_MAJOR}.abi${Python_VERSION_MAJOR}.so.${Qt6Widgets_VERSION})
if(NOT EXISTS ${pyside_shared_libraries})
	set(pyside_shared_libraries ${_Python_SITEARCH}/PySide${Qt_VERSION_MAJOR}/libpyside${Qt_VERSION_MAJOR}.abi${Python_VERSION_MAJOR}.so.${Qt_VERSION_MAJOR}.${Qt_VERSION_MINOR})
endif()
if(NOT EXISTS ${pyside_shared_libraries})
    message(FATAL_ERROR "pyside_shared_libraries not found")
endif()
message(STATUS "Found pyside_shared_libraries: ${pyside_shared_libraries}")

set(shiboken_path "${QT_HOST_PATH}/bin/shiboken6${CMAKE_EXECUTABLE_SUFFIX}")
if(NOT EXISTS ${shiboken_path})
    set(shiboken_path "${shiboken_generator_path}/shiboken6${CMAKE_EXECUTABLE_SUFFIX}")
endif()
if(NOT EXISTS ${shiboken_path})
    message(FATAL_ERROR "Shiboken executable not found at path: ${shiboken_path}")
endif()
message(STATUS "Found shiboken_path: ${shiboken_path}")


# =============================== CMake target - wiggly_library ===============================


# Get the relevant Qt include dirs, to pass them on to shiboken.
set(INCLUDES "")
get_property(INCLUDE_DIRS TARGET Qt6::Core PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
foreach(INCLUDE_DIR ${INCLUDE_DIRS})
    list(APPEND INCLUDES "-I${INCLUDE_DIR}")
endforeach()
get_property(INCLUDE_DIRS TARGET Qt6::Gui PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
foreach(INCLUDE_DIR ${INCLUDE_DIRS})
    list(APPEND INCLUDES "-I${INCLUDE_DIR}")
endforeach()
get_property(INCLUDE_DIRS TARGET Qt6::Widgets PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
foreach(INCLUDE_DIR ${INCLUDE_DIRS})
    list(APPEND INCLUDES "-I${INCLUDE_DIR}")
endforeach()

# On macOS, check if Qt is a framework build. This affects how include paths should be handled.
get_target_property(QtCore_is_framework Qt6::Core FRAMEWORK)
if (QtCore_is_framework)
    get_target_property(qt_core_library_location Qt6::Core LOCATION)
    get_filename_component(qt_core_library_location_dir "${qt_core_library_location}" DIRECTORY)
    get_filename_component(lib_dir "${qt_core_library_location_dir}/../" ABSOLUTE)
    list(APPEND INCLUDES "--framework-include-paths=${lib_dir}")
endif()

# We need to include the headers for the module bindings that we use.
set(pyside_additional_includes "")
foreach(INCLUDE_DIR ${pyside_include_dir})
    list(APPEND pyside_additional_includes "${INCLUDE_DIR}")
    list(APPEND pyside_additional_includes "${INCLUDE_DIR}/QtCore")
    list(APPEND pyside_additional_includes "${INCLUDE_DIR}/QtGui")
    list(APPEND pyside_additional_includes "${INCLUDE_DIR}/QtWidgets")
endforeach()


# ====================== Shiboken target for generating binding C++ files  ====================


# Set up the options to pass to shiboken.
set(shiboken_options --generator-set=shiboken --enable-parent-ctor-heuristic
    --enable-pyside-extensions --enable-return-value-heuristic --use-isnull-as-nb_nonzero
    --avoid-protected-hack
    ${INCLUDES}
    -I${CMAKE_CURRENT_SOURCE_DIR}/../src
    -I${CMAKE_BINARY_DIR}
    -T${CMAKE_CURRENT_SOURCE_DIR}
    -T${pyside_typesystem_dir}
    --output-directory=${CMAKE_CURRENT_SOURCE_DIR}  # todo
    )

